import { IItem } from '../types';
import * as itemsJson from '../data/items.json';

export const items: IItem[] = itemsJson.items as IItem[];
