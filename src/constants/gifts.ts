import { IGift } from '../types';
import * as giftsJson from '../data/gifts.json';

export const gifts: IGift[] = giftsJson.gifts as IGift[];
