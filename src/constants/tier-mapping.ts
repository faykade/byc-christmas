import { items } from '.';
import { ITier } from '../types';
import { getItemsForTier } from '../utils';

export const S_TIER: ITier = {
  tier: 'S',
  description: 'Shit',
  items: getItemsForTier('S', items),
  rollThreshold: 30,
};

export const A_TIER: ITier = {
  tier: 'A',
  description: 'Alright',
  items: getItemsForTier('A', items),
  rollThreshold: 40,
};

export const B_TIER: ITier = {
  tier: 'B',
  description: 'Better',
  items: getItemsForTier('B', items),
  rollThreshold: 60,
};

export const C_TIER: ITier = {
  tier: 'C',
  description: 'Cool',
  items: getItemsForTier('C', items),
  rollThreshold: 65,
};

export const D_TIER: ITier = {
  tier: 'D',
  description: 'Dapper',
  items: getItemsForTier('D', items),
  rollThreshold: 70,
};

export const F_TIER: ITier = {
  tier: 'F',
  description: 'Fantastic',
  items: getItemsForTier('F', items),
  rollThreshold: 80,
};

export const TIERS: ITier[] = [S_TIER, A_TIER, B_TIER, C_TIER, D_TIER, F_TIER];

// ['a - pets', 'd - transmog', 'b - relevant stuff', 'f - mounts/toys', 'c - collector', 's - junk'];

// [
//   ('shit - junk', 'animals - pets', 'bag filler - relevant', 'collector', 'drip - transmog', 'fun - mounts/toys')
// ];
