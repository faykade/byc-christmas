import { IWinners } from '../types';
import * as winnersJson from '../data/winners.json';

export const winners: IWinners = winnersJson as IWinners;
