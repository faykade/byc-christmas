import { IItem } from '../types';

export const getUniqueItemId = (item: IItem) => {
  return `${item.tier}-${item.id}`;
};
