import { TierOption } from '../types';
import { TIERS } from '../constants';

export const getTierThresholds = () => {
  const thresholds: Partial<Record<TierOption, number>> = {};

  TIERS.forEach((tier) => {
    thresholds[tier.tier] = tier.rollThreshold;
  });

  return thresholds;
};
