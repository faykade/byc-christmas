import { IItem, TierOption } from '../types';

export const getItemsForTier = (tier: TierOption, items: IItem[] = []) => {
  return items.filter((item) => item.tier === tier);
};
