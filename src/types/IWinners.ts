export type IWinners = {
  winners: Record<string, string[]>;
};
