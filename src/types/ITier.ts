import { TierOption } from './TierOption';
import { IItem } from './IItem';

export interface ITier {
  tier: TierOption;
  description: string;
  items: IItem[];
  rollThreshold: number;
}
