import { createContext } from 'react';

export const VisibilityContext = createContext<{ isVisible: boolean; onIsVisibleChange: () => void }>({
  isVisible: false,
  onIsVisibleChange: () => null,
});
