import { Box, Button, Typography } from '@mui/material';
import { useState } from 'react';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { VisibilityContext } from './context';

interface Props {
  children: React.ReactNode;
  showButton?: boolean;
}

export const VisibilityContextWrapper = ({ children, showButton = false }: Props) => {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <Box>
      <VisibilityContext.Provider value={{ isVisible, onIsVisibleChange: () => setIsVisible((prev) => !prev) }}>
        {showButton ? (
          <Button sx={{ margin: '20px 0px' }} variant="outlined" onClick={() => setIsVisible((prev) => !prev)}>
            {isVisible ? (
              <>
                <Typography>Hide all Results</Typography> <VisibilityOffIcon sx={{ marginLeft: '15px' }} />
              </>
            ) : (
              <>
                <Typography>Show all Results</Typography> <VisibilityIcon sx={{ marginLeft: '15px' }} />
              </>
            )}
          </Button>
        ) : null}
        {children}
      </VisibilityContext.Provider>
    </Box>
  );
};
