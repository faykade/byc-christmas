import Tier from './Tier';
import { TIERS, winners } from '../constants';
import { VisibilityContextWrapper } from '../context/VisibilityContext';
import { useMemo } from 'react';

const TierList = () => {
  const hasWinners = useMemo(() => Object.keys(winners.winners).length > 0, []);

  return (
    <VisibilityContextWrapper showButton={hasWinners}>
      {TIERS.map((tier) => (
        <Tier tier={tier} key={tier.tier} />
      ))}
    </VisibilityContextWrapper>
  );
};

export default TierList;
