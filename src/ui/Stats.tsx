import { VisibilityContextWrapper } from '../context/VisibilityContext';
import { useMemo } from 'react';
import StatsBody from './StatsBody';
import { winners } from '../constants';

const Stats = () => {
  const hasWinners = useMemo(() => Object.keys(winners.winners).length > 0, []);

  return (
    <VisibilityContextWrapper showButton={hasWinners}>
      <StatsBody />
    </VisibilityContextWrapper>
  );
};

export default Stats;
