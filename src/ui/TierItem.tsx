import { TableCell, TableRow } from '@mui/material';
import { IItem } from '../types';
import { useContext, useEffect, useMemo, useState } from 'react';
import { winners } from '../constants';
import Visibility from './Visibility';
import { getUniqueItemId } from '../utils';
import { VisibilityContext } from '../context/VisibilityContext';

interface Props {
  item: IItem;
}

const TierItem = ({ item }: Props) => {
  const [showWinners, setShowWinners] = useState(false);
  const winningUsers = useMemo(() => winners.winners[getUniqueItemId(item)] || [], [item]);
  const { isVisible } = useContext(VisibilityContext);

  useEffect(() => {
    setShowWinners(isVisible);
  }, [isVisible]);

  return (
    <TableRow>
      <TableCell>{item.id}</TableCell>
      <TableCell>
        <a
          style={item.remainingQuantity > 0 ? {} : { textDecoration: 'line-through', opacity: 0.5 }}
          href={`https://www.wowhead.com/item=${item.id}`}
        ></a>{' '}
        ({item.giftQuantity})
      </TableCell>
      <TableCell>
        {winningUsers && winningUsers.length > 0 ? (
          <>
            <Visibility isVisible={showWinners} onChangeVisible={() => setShowWinners((prev) => !prev)} />
            {showWinners ? winningUsers.join(', ') : null}
          </>
        ) : null}
      </TableCell>
      <TableCell sx={{ textAlign: 'right' }}>{item.remainingQuantity}</TableCell>
    </TableRow>
  );
};

export default TierItem;
