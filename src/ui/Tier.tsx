import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@mui/material';
import { ITier } from '../types';
import { useEffect, useState } from 'react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import TierItem from './TierItem';

interface Props {
  tier?: ITier;
  defaultExpanded?: boolean;
}

const Tier = ({ tier, defaultExpanded = false }: Props) => {
  const [expanded, setExpanded] = useState(defaultExpanded);

  useEffect(() => {
    if (expanded && $WowheadPower) {
      $WowheadPower.refreshLinks();
    }
  }, [expanded]);

  return (
    <Accordion variant="outlined" expanded={expanded} onChange={() => setExpanded((prev) => !prev)}>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        {tier?.description} Tier - ({tier?.tier}) - Beat {tier?.rollThreshold}
      </AccordionSummary>
      <AccordionDetails>
        <Box sx={{ overflowX: 'auto' }}>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>
                  <Box width="80px">ID</Box>
                </TableCell>
                <TableCell>
                  <Box minWidth="200px">Item (Gift Qty)</Box>
                </TableCell>
                <TableCell>
                  <Box width="200px">Winner(s)</Box>
                </TableCell>
                <TableCell>
                  <Box width="80px">Remaining</Box>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{tier?.items.map((item) => <TierItem key={item.id} item={item} />)}</TableBody>
          </Table>
        </Box>
      </AccordionDetails>
    </Accordion>
  );
};

export default Tier;
