import { Box, Container, Paper } from '@mui/material';

import TierList from './TierList';
import Description from './Description';
import GiftsDescription from './GiftsDescription';
import Gifts from './Gifts';
import TierDescription from './TiersDescription';
import carryImage from '../assets/carry-presents.png';
import StatsDescription from './StatsDescription';
import Stats from './Stats';

const App = () => {
  return (
    <Container maxWidth="lg">
      <Paper elevation={3} sx={{ margin: '15px 0px', padding: '15px' }}>
        <Description />
        <Box display="flex" alignItems="center" justifyContent="center">
          <img style={{ maxWidth: '75%' }} src={carryImage} alt="" />
        </Box>
      </Paper>
      <Paper elevation={3} sx={{ margin: '15px 0px', padding: '15px' }}>
        <TierDescription />
        <TierList />
      </Paper>
      <Paper elevation={3} sx={{ margin: '30px 0px', padding: '15px' }}>
        <GiftsDescription />
        <Gifts />
      </Paper>
      <Paper elevation={3} sx={{ margin: '30px 0px', padding: '15px' }}>
        <StatsDescription />
        <Stats />
      </Paper>
    </Container>
  );
};

export default App;
