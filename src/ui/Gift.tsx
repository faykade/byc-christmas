import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';
import { IGift } from '../types';
import { useState, useEffect, useMemo, useContext } from 'react';
import Visibility from './Visibility';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { getTierThresholds } from '../utils';
import { VisibilityContext } from '../context/VisibilityContext';

interface Props {
  gift: IGift;
}

const Gift = ({ gift }: Props) => {
  const [expanded, setExpanded] = useState(false);
  const [showWinnings, setShowWinnings] = useState(false);
  const [showRolls, setShowRolls] = useState(false);
  const [showTiers, setShowTiers] = useState(false);
  const { isVisible } = useContext(VisibilityContext);

  useEffect(() => {
    if (showWinnings && $WowheadPower) {
      $WowheadPower.refreshLinks();
    }
  }, [showWinnings]);

  useEffect(() => {
    setShowWinnings(isVisible);
    setShowRolls(isVisible);
    setShowTiers(isVisible);
  }, [isVisible]);

  const thresholds = useMemo(() => getTierThresholds(), []);

  const wonTiers = useMemo(() => gift.rolls.filter((roll) => roll.winner).map((item) => item.tierOption), [gift.rolls]);

  return (
    <Accordion variant="outlined" expanded={expanded} onChange={() => setExpanded((prev) => !prev)}>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>{gift.username}</AccordionSummary>
      <AccordionDetails>
        <Box display="flex" alignItems="center">
          <Typography variant="body2">Winning Items</Typography>{' '}
          <Visibility isVisible={showWinnings} onChangeVisible={() => setShowWinnings((prev) => !prev)} />
        </Box>
        {showWinnings ? (
          <ul>
            {gift.wins.map((item) => (
              <li key={item.id}>
                {item.tier} - <a href={`https://www.wowhead.com/item=${item.id}`}></a> ({item.giftQuantity})
              </li>
            ))}
          </ul>
        ) : null}
        <Box display="flex" alignItems="center">
          <Typography variant="body2">Winning Tiers</Typography>{' '}
          <Visibility isVisible={showTiers} onChangeVisible={() => setShowTiers((prev) => !prev)} />
        </Box>
        {showTiers ? wonTiers.join(', ') : null}
        <Box display="flex" alignItems="center">
          <Typography variant="body2">Rolls</Typography>{' '}
          <Visibility isVisible={showRolls} onChangeVisible={() => setShowRolls((prev) => !prev)} />
        </Box>
        {showRolls ? (
          <Box sx={{ overflowX: 'auto' }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Roll #</TableCell>
                  <TableCell>Tier</TableCell>
                  <TableCell># to Beat</TableCell>
                  <TableCell>{gift.username}'s roll</TableCell>
                  <TableCell>Winner</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {gift.rolls.map((roll, index) => {
                  const isDifferentRoll = index === 0 || gift.rolls[index - 1].roll !== roll.roll;
                  return (
                    <TableRow key={index}>
                      <TableCell sx={isDifferentRoll ? { borderTop: '2px solid white' } : {}}>{index + 1}</TableCell>
                      <TableCell sx={isDifferentRoll ? { borderTop: '2px solid white' } : {}}>
                        {roll.tierOption}
                      </TableCell>
                      <TableCell sx={isDifferentRoll ? { borderTop: '2px solid white' } : {}}>
                        {thresholds[roll.tierOption]}
                      </TableCell>
                      <TableCell sx={isDifferentRoll ? { borderTop: '2px solid white' } : {}}>{roll.roll}</TableCell>
                      <TableCell sx={isDifferentRoll ? { borderTop: '2px solid white' } : {}}>
                        {roll.winner ? 'Yes' : null}
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        ) : null}
      </AccordionDetails>
    </Accordion>
  );
};

export default Gift;
