import { Box, Typography } from '@mui/material';

const StatsDescription = () => {
  return (
    <Box>
      <Typography variant="h4" sx={{ textAlign: 'center' }}>
        Stats
      </Typography>
      <Box sx={{ margin: '20px 0px' }}>
        <Typography variant="body1">
          This is the area where you can see aggregated stats of how the rolls turned out. I might do something with
          this later, but for now it is just some fun information.
        </Typography>
      </Box>
    </Box>
  );
};

export default StatsDescription;
