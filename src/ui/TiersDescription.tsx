import { Box, Typography } from '@mui/material';
import { MIN_WINS } from '../constants/app';
import { getTierThresholds } from '../utils';
import { TierOption } from '../types';
import { useCallback, useMemo } from 'react';

const EXAMPLE_FIRST_ROLL = 42;
const EXAMPLE_SECOND_ROLL = 68;
const TIER_THRESHOLDS = getTierThresholds();

const TierDescription = () => {
  const getWonTiers = useCallback((roll: number) => {
    return Object.keys(TIER_THRESHOLDS).filter((key) => {
      const currentTierThreshold = TIER_THRESHOLDS[key as TierOption] || 0;
      return currentTierThreshold < roll;
    });
  }, []);

  const initialWins = useMemo(() => getWonTiers(EXAMPLE_FIRST_ROLL), [getWonTiers]);

  const secondWins = useMemo(() => {
    return getWonTiers(EXAMPLE_SECOND_ROLL).filter((tier) => !initialWins.includes(tier));
  }, [getWonTiers, initialWins]);

  return (
    <Box>
      <Typography variant="h4" sx={{ textAlign: 'center' }}>
        Loot Table
      </Typography>
      <Box sx={{ margin: '20px 0px' }}>
        <Typography variant="body1">
          The way this works is that you will win at least {MIN_WINS} gifts, but at most 1 from any given loot tier.
          I've got a script that rolls a number for you between 1-100 and checks if your roll is higher than each tier's
          roll threshold. If it is, you are gifted a random item from that tier. For example, if you roll{' '}
          {EXAMPLE_FIRST_ROLL}, you would win items from the following tiers: {initialWins.join(', ')}. That doesn't
          reach 3 total gifts though, so we would roll again. If your second roll was {EXAMPLE_SECOND_ROLL}, you would
          win new items from the {secondWins.join(', ')} tiers along with your first roll winnings (
          {initialWins.join(', ')}). This is more than the {MIN_WINS} threshold, so your roll would be complete.
        </Typography>
        <br />
        <Typography variant="body1">
          There are only a certain amount of each item available, I'll add more stuff if any tier gets short on
          rewards/quantities. I also give you the option to see who won which items and how many are left. If you don't
          want to ruin the surprise, you might not want to check the winner's list if you've already submitted your name
          but not received your gift.
        </Typography>
        <br />
        <Typography variant="body1">
          <em>TLDR</em>: At best you could win 1 item from every tier, at worst you would win something from the bottom{' '}
          {MIN_WINS} tiers. Do whatever you want with the gift, if you can't/don't want to use it you can trade it or
          sell it, up to you.
        </Typography>
      </Box>
    </Box>
  );
};

export default TierDescription;
