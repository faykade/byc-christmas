import { Box, Typography } from '@mui/material';

const Description = () => {
  return (
    <Box>
      <Typography variant="h4" sx={{ textAlign: 'center' }}>
        A Boralus Yacht Club Festivity
      </Typography>
      <Box sx={{ margin: '20px 0px' }}>
        <Typography variant="body1">
          This is just a fun little project for the holidays where I'll give away some gifts to BYC members. Here is a
          loot tier list, just let me know if you want to have your name entered for a gift. You can contact me
          (Progressed-Alterac Mountains) in game (can be in person or mail), or shoot me a message in discord and I'll
          add you to the gift list. When you contact me, please tell me the character you want me to mail the gifts to
          afterwards. (Must be able to send mail to your character from our server group)
        </Typography>
        <br />
      </Box>
    </Box>
  );
};

export default Description;
