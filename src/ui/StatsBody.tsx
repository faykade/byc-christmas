import { Box, Typography } from '@mui/material';
import { MIN_WINS } from '../constants/app';
import { TIERS, gifts } from '../constants';
import { IGift } from '../types';
import { useContext } from 'react';
import { VisibilityContext } from '../context/VisibilityContext';

const GROUPS: IGift[][] = [];
for (let i = MIN_WINS - 1; i < TIERS.length; i++) {
  GROUPS.push(gifts.filter((gift) => gift.wins.length === i + 1));
}

const StatsBody = () => {
  const { isVisible } = useContext(VisibilityContext);
  return isVisible ? (
    <Box>
      <ul>
        {GROUPS.map((gifts, index) => (
          <li key={index}>
            <Typography>
              {TIERS[index + MIN_WINS - 1].description} Rolls ({index + MIN_WINS} gifts)
            </Typography>
            <ul>
              {gifts.map((gift, index) => (
                <li key={index}>{gift.username}</li>
              ))}
            </ul>
          </li>
        ))}
      </ul>
    </Box>
  ) : null;
};

export default StatsBody;
