import { Box, Typography } from '@mui/material';

const GiftsDescription = () => {
  return (
    <Box>
      <Typography variant="h4" sx={{ textAlign: 'center' }}>
        Gifts
      </Typography>
      <Box sx={{ margin: '20px 0px' }}>
        <Typography variant="body1">
          This is the area where you can see the current gifts that have already been won. I'll update this area
          periodically. I didn't want to spend too much time writing this application, so it is kind of a manual process
          of moving files around. I've hidden the results in case you don't want to ruin the surprise, but you can view
          them if you want by turning on/off the eyeball
        </Typography>
      </Box>
    </Box>
  );
};

export default GiftsDescription;
