import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { IconButton } from '@mui/material';

interface Props {
  isVisible?: boolean;
  onChangeVisible?: () => void;
}

const Visibility = ({ isVisible = false, onChangeVisible = () => null }: Props) => {
  return <IconButton onClick={onChangeVisible}>{isVisible ? <VisibilityOffIcon /> : <VisibilityIcon />}</IconButton>;
};

export default Visibility;
