import { useMemo, useState } from 'react';
import { gifts } from '../constants';
import { VisibilityContextWrapper } from '../context/VisibilityContext';
import Gift from './Gift';
import { Box, Pagination } from '@mui/material';
import { IGift } from '../types';

const PAGE_SIZE = 10;

const Gifts = () => {
  const [page, setPage] = useState(1);

  const totalPages = useMemo(() => Math.ceil(gifts.length / PAGE_SIZE), []);

  const currentGifts = useMemo((): IGift[] => {
    const startPosition = (page - 1) * PAGE_SIZE;
    const endPosition = startPosition + PAGE_SIZE;
    return gifts.slice(startPosition, endPosition);
  }, [page]);

  return (
    <VisibilityContextWrapper showButton={gifts.length > 0}>
      {currentGifts.map((gift) => (
        <Gift key={gift.username} gift={gift} />
      ))}
      {totalPages > 1 ? (
        <Box margin="15px 0px" display="flex" justifyContent="center">
          <Pagination onChange={(_, pageNum) => setPage(pageNum)} count={totalPages} page={page} variant="outlined" />
        </Box>
      ) : null}
    </VisibilityContextWrapper>
  );
};

export default Gifts;
