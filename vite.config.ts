import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  base: '/byc-christmas',
  plugins: [react()],
  build: {
    outDir: 'public',
    emptyOutDir: true,
  },
  publicDir: 'assets',
});
